const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const { articleSchema } = require('./Article')

const bookSchema = new Schema({
    name: {
        type: String
    },

    author: {
        type: String
    },

    articles: [
        {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Article'
        }

    ],

    createdAt: {
        type: Date,
        default: Date.now()
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }

})









const Book = mongoose.model('Book', bookSchema);
exports.Book = Book;
