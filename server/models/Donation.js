const mongoose = require('mongoose')
const Schema = mongoose.Schema;




const donationSchema = new Schema({

    donor: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Donor'
    },
    amount: {
        type: Number,
        default: 0
    },

    donatedAt: {
        type: Date,
        default: Date.now()
    }




})



const Donation = new mongoose.model('Donation', donationSchema)

module.exports = { Donation }