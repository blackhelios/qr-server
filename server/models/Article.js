const mongoose = require('mongoose')
const Schema = mongoose.Schema;


const articleSchema = new Schema({

    title: {
        type: Number
    },

    subtitle: {
        type: Number
    },

    body: {
        type: String
    },


    articleLink: {
        link: {
            type: String
        },
        title: {
            type: Number
        },

        subtitle: {
            type: Number
        }

    },

    images: {
        type: Array,
        default: []
    },

    book: {
        type: Schema.Types.ObjectId,
        ref: 'Book'
    },

    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }

    // TO-DO AUDIO PROPERTY





})



const Article = mongoose.model('Article', articleSchema);

exports.articleSchema = articleSchema;
exports.Article = Article;
