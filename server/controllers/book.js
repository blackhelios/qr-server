const { Book } = require('../models/Book');


exports.addBook = function (req, res) {

    const { name, author } = req.body;

    const creator = req.user._id;



    const newBook = new Book({
        name,
        author,
        creator,
        articles: []
    })


    newBook.save((err, book) => {

        if (err) {
            return res.status(500).send({ success: false, message: 'erorr in creating a book' })
        }

        res.status(200).send({ success: true, book: book })
    })

}


exports.getAllBooks = (req, res) => {

    Book.find({}, (err, books) => {

        if (err) {
            res.status(500).send({ error: err, message: 'error occurs when querying all books' })
        }

        res.send({ success: true, books: books })

    })



}


exports.editBook = (req, res) => {

    const { bookId } = req.params;


    Book.findOneAndUpdate({ _id: bookId }, { $set: req.body }, { new: true, upsert: true }, (err, book) => {
        if (err) {
            res.status(500).send({ error: err, message: 'problem in updating book' })

        }

        res.status(200).send({ success: true, book: book })
    })


}


exports.deleteBook = (req, res) => {

    const { bookId } = req.params;

    Book.findOneAndDelete({ _id: bookId }, (err, book) => {
        if (err) {
            res.status(500).send({ error: err, message: 'error deleting book' })
        }

        res.status(200).send({ success: true })
    })



}