const { User } = require('../models/User');
const normailizeErrors = require('../services/mongoose')

exports.register = function (req, res) {

    let { email, password, username } = req.body;

    if (!email || !password) {
        return res.status(422).send({ errors: [{ title: 'Data missing!', detail: 'Provide email and password!' }] });
    }

    const user = new User({

        local: {
            username: username,
            email: email,
            password: password
        }

    })

    user.save((err, userDoc) => {
        if (err) { return res.json({ success: false, errors: normailizeErrors(err.errors) }) }

        res.status(500).json({ success: true })

    })
}






exports.login = function (req, res) {


    User.findOne({ 'local.email': req.body.email }, (err, user) => {
        if (!user) {
            return res.status(422).send({ success: false, message: 'Auth Failed , user not found ' })
        }

        if (err) {
            return res.status(422).send({ message: err.message });
        }

        user.comparePassword(req.body.password, (err, isMatch) => {
            if (err) {
                return res.status(422).send({ message: err.message });
            }
            if (!isMatch) {
                return res.json({ success: false, message: 'Wrong password' })
            }


            user.generateToken((err, user) => {
                if (err) { return res.status(400).send(err) }

                console.log(user.token)
                res.send({ success: true, token: user.token })

            })

        })


    })



}


exports.fbLogin = function (req, res) {

    const { email, name, accessToken } = req.body;

    // TO-DO
    // what if user change email in facebook ?? 
    // check accessToken and it matches but no email , we would replace token and give access to it .



    User.findOne({ 'facebook.email': email }, (err, user) => {

        if (!user) {
            const user = new User({

                facebook: {
                    username: name,
                    email: email,
                    accessToken: accessToken
                }

            })

            user.save((err, userDoc) => {
                if (err) { return res.json({ success: false, errors: normailizeErrors(err.errors) }) }

                // res.status(500).json({ success: true })
                userDoc.generateToken((err, user) => {


                    if (err) {
                        return res.status(400).send(err)
                    }


                    res.send({ loginSuccess: true, token: user.token })

                })
            })
        } else {
            res.send({ loginSuccess: true, token: user.token })
        }









    })

}



exports.auth = function (req, res) {


    if (!req.user.facebook) {
        res.status(200).json({
            email: req.user.local.email,
            username: req.user.local.username,
            isAuth: true,
            authType: 'local',
            role: req.user.role


        })

    } else {

        res.status(200).json({
            email: req.user.facebook.email,
            username: req.user.facebook.username,
            isAuth: true,
            authType: 'facebook',
            role: req.user.role


        })
    }





}


exports.logout = function (req, res) {

    User.findOneAndUpdate({ _id: req.user._id }, { token: '' }, (err, updateUser) => {


        if (err) { return res.json({ success: false, err }) }
        return res.status(200).send({
            success: true
        })
        console.log('success')

    })




}