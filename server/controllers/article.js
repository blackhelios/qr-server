

const { Book } = require('../models/Book')
const { Article } = require('../models/Article')
const config = require('../config/index')




exports.addArticle = function (req, res) {





    const { book, title, subtitle, body } = req.body;
    const images = req.body.images;
    const creator = req.user._id
    const articleLink = {
        title: req.body.articleLink.title,
        subtitle: req.body.articleLink.subtitle,
        link: `${config.FRONTEND_URL}/?${title}&${subtitle}`

    }




    const newArticle = new Article({
        title,
        subtitle,
        body,
        articleLink,
        images,
        book: book,
        creator: creator

    })



    // 1st find article by  title , subtitle , book 

    Article.findOne({ title: title, subtitle: subtitle, book: book }, (err, article) => {

        if (err) {
            res.status(500).send({ error: err })
        }

        if (!article) {
            // !exist   >>  save newArticle >> push article _id into book articles array  >> return newArticle to client-side
            newArticle.save((err, article) => {

                // push article id into book articles array for future populate
                Book.findByIdAndUpdate(book, { $push: { 'articles': article._id } }, (err, book) => {
                    res.status(200).send({ success: true, article: article })
                })



            })
        } else {
            // if exist >> return error message
            res.status(500).send({ success: false, message: 'article already exists' })
        }








    })




}




exports.searchArticle = (req, res) => {
    const { book, title, subtitle } = req.body;


    // 1st find article by  title , subtitle , book 

    Article.findOne({ title: title, subtitle: subtitle, book: book }, (err, article) => {

        if (err) {
            res.statu(500).send({ error: err })
        }
        if (!article) {

            res.json({ success: false, message: 'article u are finding seems not added yet. we are adding data' })


        } else {
            res.status(200).send({
                success: true,
                article: article
            })
        }








    })





}




exports.editArticle = (req, res) => {

    const { articleId } = req.params;


    console.log(req.body)


    Article.findOneAndUpdate({ _id: articleId }, { $set: req.body }, { new: true, upsert: true }, (err, article) => {
        if (err) {
            res.status(500).send({ error: err, message: 'problem in updating aticle' })

        }

        res.status(200).send({ success: true, article: article })
    })


}


exports.deleteArticle = (req, res) => {

    const { articleId } = req.params;

    Article.findOneAndDelete({ _id: articleId }, (err, article) => {
        if (err) {
            res.status(500).send({ error: err, message: 'error deleting article' })
        }

        res.status(200).send({ success: true })
    })



}