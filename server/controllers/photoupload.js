const upload = require('../services/s3-upload')
const singleUpload = upload.single('image')

const cloudinary = require('cloudinary');
const config = require('../config')

cloudinary.config({
    cloud_name: config.CD_CLOUD_NAME,
    api_key: config.CD_CLOUD_API_KEY,
    api_secret: config.CD_CLOUD_API_SECRET
});


exports.S3photoUpload = function (req, res) {


    singleUpload(req, res, (err) => {

        if (err) {
            return res.status(422).send({ err: err });
        }

        return res.json({ 'imageUrl': req.file.location });



    })


}


exports.CDphotoupload = function (req, res) {


    cloudinary.uploader.upload(req.files.file.path, (result) => {

        console.log(result)
        res.status(200).send({
            success: true,
            public_id: result.public_id,
            url: result.url
        })

    },
        {
            public_id: `${Date.now()}`,
            resource_type: 'auto'
        }

    )





}



exports.CDimagedelete = (req, res) => {

    const { imageId } = req.params;


    cloudinary.uploader.destroy(imageId, (error, result) => {
        if (error) return res.json({ succes: false, error });
        res.status(200).send({ success: true });
    })



}