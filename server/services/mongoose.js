
 function normalizeErrors (errors) {

    let normalize_Errors = [];

    for(let property in errors) {

            if(errors.hasOwnProperty(property)) {

                normalize_Errors.push({title : property , details : errors[property].message})

            }

    }

    return normalize_Errors;


}

module.exports = normalizeErrors;