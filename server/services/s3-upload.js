const aws = require('aws-sdk')
const multer = require('multer')
const multerS3 = require('multer-s3')
const config = require('../config')

aws.config.update({
    secretAccessKey: config.AWS_SECRET_KEY,
    accessKeyId: config.AWS_ACCESS_KEY_ID,
    region: 'us-east-2',

})


const s3 = new aws.S3();

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(new Error('Invalid file type, only JPEG and PNG is allowed!'), false);
    }
}

const upload = multer({
    fileFilter,
    storage: multerS3({
        acl: 'pubic-read',
        s3: s3,
        bucket: 'qr-reader',
        metadata: function (req, file, cb) {
            // s3 url link sent back 
            cb(null, { fieldName: 'Testing metadata' });

        },
        key: function (req, file, cb) {
            cb(null, Date.now().toString())


        }
    })
})


module.exports = upload;