const express = require('express')
const router = express.Router()
const bookCtrl = require('../../controllers/book')

const { authMiddleware } = require('../../middleware/auth')
const { check, validationResult } = require('express-validator/check');




router.post('/add', authMiddleware, bookCtrl.addBook)
router.get('/all',  bookCtrl.getAllBooks)
router.post('/edit/:bookId', authMiddleware, bookCtrl.editBook)
router.post('/delete/:bookId', authMiddleware, bookCtrl.deleteBook)

module.exports = router;