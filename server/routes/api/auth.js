const express = require('express');
const passport = require('passport');
const router = express.Router();
const keys = require('../../config/index');
const { User } = require('../../models/User');
const jwt = require('jsonwebtoken');
const config = require('../../config/index')


// FACEBOOK OAUTH2 ROUTE
router.get('/facebook', passport.authenticate('facebook', {
    session: false,
    scope: ['email']

}));

router.get(
    '/facebook/callback',
    // 
    passport.authenticate('facebook', { failureRedirect: '/', session: false }),
    (req, res) => {


        const payload = req.user._id.toHexString();

        jwt.sign(
            payload,
            'yoursecret',
            (err, token) => {

                console.log(token);
                if (req.user.token) {
                    res.cookie('auth', req.user.token, { httpOnly: false, maxAge: 365 * 24 * 60 * 60 * 1000 })
                    // res.send({ token: token, loginSuccess: true })
                } else {
                    req.user.token = token;
                    req.user.save((err, user) => {
                        console.log('success')
                    })
                    res.cookie('auth', token, { httpOnly: false, maxAge: 365 * 24 * 60 * 60 * 1000 })
                    // res.send({ token: token, loginSuccess: true })
                }



                res.redirect(`${config.FRONTEND_URL}/user`)





            }

        )



    }

);







module.exports = router;