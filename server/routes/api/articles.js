const express = require('express');
const router = express.Router()
const { check, validationResult } = require('express-validator/check');
const { authMiddleware } = require('../../middleware/auth')
const formiddable = require('express-formidable')
const articlesCtrl = require('../../controllers/article')
const imageUploadCtrl = require('../../controllers/photoupload')

router.post('/add', authMiddleware, articlesCtrl.addArticle)
router.post('/search', articlesCtrl.searchArticle)
router.post('/edit/:articleId', authMiddleware, articlesCtrl.editArticle)
router.post('/delete/:articleId', authMiddleware, articlesCtrl.deleteArticle)
// router.post('/image', authMiddleware ,  imageUploadCtrl.S3photoUpload)

router.post('/image', authMiddleware, formiddable(), imageUploadCtrl.CDphotoupload)
router.post('/image/:imageId', authMiddleware, imageUploadCtrl.CDimagedelete)

module.exports = router;