const passport = require('passport');
const express = require('express');


const userRoutes = require('./api/user')
const bookRoutes = require('./api/book')
const articlesRoutes = require('./api/articles')


const cookieParser = require('cookie-parser')
const authRoutes = require('./api/auth')
const cors = require('cors')
const config = require('../config/index.js')


module.exports = function (app) {
    // third party middlewares 
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))


    const corsOptions = {
        origin: config.FRONTEND_URL,
        credentials: true
    }

    app.use(cors(corsOptions))
    app.use(passport.initialize())
    app.use(cookieParser());

    app.get('/', (req, res) => {

        res.json({
            message: 'Quren Reader API Server',
            phone: '09 968717100',
            developer: 'thuta'

        })
    }
    )
    // api middlewares 

    app.use('/api/v1/users', userRoutes)
    app.use('/api/v1/books', bookRoutes)
    app.use('/api/v1/articles', articlesRoutes)
    app.use('/auth', authRoutes)

}