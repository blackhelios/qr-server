
// pm2 config file for server setup
module.exports = {


    app : [

            {
                name : './server/server.js' ,
                watch : true ,
                env : {
                    "NODE_ENV" : "development"
                },

                env_production : {
                    "NODE_ENV" : "production" ,
                    "FACEBOOK_APP_ID": "" ,
                    "FACEBOOK_APP_SECRET" : "" ,
                    "MONGO_URI" : "" ,
                    "SECRET_OR_KEY" : ''
                }
            }

        
    ]





}